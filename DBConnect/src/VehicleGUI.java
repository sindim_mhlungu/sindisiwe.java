import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*; 

//********************************************************************************************************************************************************************************
     class VehicleGUI extends JPanel implements ListSelectionListener{
//		Declare layout & constraints
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();

//		Create Labels
		JLabel jlVehicleNum = new JLabel("VehicleNo:");  
		JLabel jlMake = new JLabel("Make:");
		JLabel jlModel = new JLabel("Model:");         
		JLabel jlRegistration = new JLabel("Registration:");         
		JLabel jlRoute = new JLabel("Route:");

//		List
		public static JList results = new JList();

//		Create Text Fields
		public static JTextField taVNumder = new JTextField(20);         
		public static JTextField taMake = new JTextField(20);       
		public static JTextField taModel = new JTextField(20);          
		public static JTextField taRegistration = new JTextField(20);
	    public static JTextField taRoute = new JTextField(20);

//		Create Buttons
		JButton jbPrev = new JButton("<<");     
		JButton jbNext = new JButton(">>");
		JButton jbSave = new JButton("Save");
		JButton jbCancel = new JButton("Cancel");
		JButton jbAdd = new JButton("Add Vehicle"); 
		JButton jbDelete = new JButton("Delete Vehicle");
		JButton jbEdit = new JButton("Edit Vehicle");      
		JButton jbSearch = new JButton("Search");         

		JScrollPane jsp = new JScrollPane(results);
//		Variables
		String sqlCommand = "";                             String state = "";
		ResultSet rs;
		DBConn dbc = new DBConn();
		void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
		{
			gbc.gridx = gx;
			gbc.gridy = gy;
			
			gbc.gridwidth = gw;
			gbc.gridheight = gh;
			
			gbc.weightx = wx;
			gbc.weighty = wy;
			gbc.anchor = GridBagConstraints.EAST;
			gbc.fill = GridBagConstraints.BOTH ;
		}//
//	****************************************************************************************
		VehicleGUI()
		{
			//Set Gridbag Layout
			this.setLayout(gbl);

			//Layout components
			layoutLabels();
			layoutTextBoxes();
			layoutButtons();

			this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Vehicle Information"));

			results.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Search Results"));

			setVisible(true);

			showFirstRecord();
			//enableDisableTextboxes();
		}
//	****************************************************************************************
		void layoutLabels()
		{
			//jlVehicleNum
			buildConstraints(gbc,0,0,1,1,0,0);
			gbl.setConstraints(jlVehicleNum,gbc);
			this.add(jlVehicleNum);

			//jlMake
			buildConstraints(gbc,0,1,1,1,0,0);
			gbl.setConstraints(jlMake,gbc);
			this.add(jlMake);

			//jlModel
			buildConstraints(gbc,0,2,1,1,0,0);
			gbl.setConstraints(jlModel,gbc);
			this.add(jlModel);

			//jlRegistration
			buildConstraints(gbc,0,3,1,1,0,0);
			gbl.setConstraints(jlRegistration,gbc);
			this.add(jlRegistration);

			//jlRoute
			buildConstraints(gbc,0,4,1,1,0,0);
			gbl.setConstraints(jlRoute,gbc);
			this.add(jlRoute);

		}
//	****************************************************************************************
		void layoutTextBoxes()
		{
			//taVNumder
			buildConstraints(gbc,1,0,2,1,10,0);
			gbl.setConstraints(taVNumder,gbc);
			this.add(taVNumder);

			//taMake
			buildConstraints(gbc,1,1,2,1,10,0);
			gbl.setConstraints(taMake,gbc);
			this.add(taMake);

			//jtaModel
			buildConstraints(gbc,1,2,2,1,10,0);
			gbl.setConstraints(taModel,gbc);
			this.add(taModel);

			//taRegistration
			buildConstraints(gbc,1,3,2,1,10,0);
			gbl.setConstraints(taRegistration,gbc);
			this.add(taRegistration);

			//taRoute
			buildConstraints(gbc,1,4,2,1,10,0);
			gbl.setConstraints(taRoute,gbc);
			this.add(taRoute);
		}
//	****************************************************************************************
		void layoutButtons()
		{
			//jbPrev
			buildConstraints(gbc,1,5,1,1,10,0);
			gbl.setConstraints(jbPrev,gbc);
			this.add(jbPrev);
			jbPrev.addActionListener(new jbPrev_Click());
			jbPrev.setMnemonic('<');
			jbPrev.setEnabled(false);

			//jbNext
			buildConstraints(gbc,2,5,1,1,10,0);
			gbl.setConstraints(jbNext,gbc);
			this.add(jbNext);
			jbNext.addActionListener(new jbNext_Click());
			jbNext.setMnemonic('>');
			
			//jbSave
			buildConstraints(gbc,1,6,1,1,10,0);
			gbl.setConstraints(jbSave,gbc);
			this.add(jbSave);
			jbSave.addActionListener(new jbSave_Click());
			jbSave.setMnemonic('S');
			jbSave.setEnabled(false);
			
      		//jbCancel
			buildConstraints(gbc,2,6,1,1,10,0);
			gbl.setConstraints(jbCancel,gbc);
			this.add(jbCancel);
			jbCancel.addActionListener(new jbCancel_Click());
			jbCancel.setMnemonic('C');
			jbCancel.setEnabled(false);


			//jbAdd
			buildConstraints(gbc,0,7,1,1,10,0);
			gbl.setConstraints(jbAdd,gbc);
			this.add(jbAdd);
			jbAdd.addActionListener(new jbAdd_Click());
			jbAdd.setMnemonic('A');

			//jbDelete
			buildConstraints(gbc,1,7,1,1,10,0);
			gbl.setConstraints(jbDelete,gbc);
			this.add(jbDelete);
			jbDelete.addActionListener(new jbDelete_Click());
			jbDelete.setMnemonic('D');

			//jbEdit
			buildConstraints(gbc,2,7,1,1,12,0);
			gbl.setConstraints(jbEdit,gbc);
			this.add(jbEdit);
			jbEdit.addActionListener(new jbEdit_Click());
			jbEdit.setMnemonic('E');

			//jbSearch
			buildConstraints(gbc,3,7,1,1,10,0);
			gbl.setConstraints(jbSearch,gbc);
			this.add(jbSearch);
			jbSearch.addActionListener(new jbSearch_Click());
			jbSearch.setMnemonic('r');

			//result
			buildConstraints(gbc,0,8,4,1,10,10);
			gbl.setConstraints(jsp,gbc);
			this.add(jsp);
			results.addListSelectionListener(this);
		}
//	****************************************************************************************
		public boolean validateInfo()
		{
			boolean empty = false;
			JTextField[] myTextBoxes = {taVNumder,taMake,taModel,taRegistration,taRoute};
			for(int count=0; count<myTextBoxes.length; ++count)
			{
				if(myTextBoxes[count].getText().equals(""))
				{
					empty = true;
				}
			}
			return empty;
		}
//	****************************************************************************************
		void addVehicle()
		{
			try
			{
				String vehiNum = taVNumder.getText();
				int rows = 0;
				int serviceID = 1;

				//Get the number of Vehicles to insert for.
				sqlCommand = "SELECT COUNT(*)AS serviceRows FROM Service";
				ResultSet rs = dbc.select(sqlCommand);
				while(rs.next())
				{
					rows = rs.getInt("serviceRows");
				}

				sqlCommand = "INSERT INTO Service VALUES (?,?,?,?,?,?,?)";
				PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);

				//Loop entering Service according to how many Vehicles are there
				for(int count=0; count < rows; ++count)
				{
					prep.setInt(1,Integer.parseInt(vehiNum));
					prep.setInt(2,0);
					prep.setInt(3,0);
					prep.setInt(4,0);
					prep.setInt(5,0);
					prep.setInt(6,serviceID);

					if(prep.executeUpdate() != 1)
					{
						throw new Exception("Insert not successful");
					}
					serviceID += 1;
				}
				JOptionPane.showMessageDialog(this,"The record was saved sucessfully","Vehicle Service",JOptionPane.OK_OPTION+JOptionPane.INFORMATION_MESSAGE);
			}
			catch(Exception exc)
			{
				JOptionPane.showMessageDialog(this,"Error:"+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
			}
		}
//	****************************************************************************************
		public void showFirstRecord()
		{
			try
			{
				dbc.connect();
				sqlCommand = "SELECT * FROM Vehicle";
				PreparedStatement pst = dbc.con.prepareStatement(sqlCommand,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

				rs = pst.executeQuery();
				if(rs.next())
				{
					taVNumder.setText(rs.getString("VehicleNo"));
					taMake.setText(rs.getString("Make"));
					taModel.setText(rs.getString("Model"));
					taRegistration.setText(rs.getString("Registration"));
					taRoute.setText(rs.getString("Route"));
				}
			}
			catch(Exception exc)
			{
				JOptionPane.showMessageDialog(this,"Error:"+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
			}
		}
//	****************************************************************************************
		public void enableANDDisableTextboxes()
		{
			try
			{
				JTextField[] myTextBoxes = {taVNumder,taMake,taModel,taRegistration,taRoute};
				//Enable TextBoxes
				for (int count = 0;count<myTextBoxes.length;++count)
				{
					if(myTextBoxes[count].isEnabled())
					{
						myTextBoxes[count].setEnabled(true);
						results.setEnabled(true);

						showFirstRecord();
					}
					else
					{
						myTextBoxes[count].setEnabled(true);
						results.setEnabled(false);
						
					if(state.equals("Add"))
					{
						myTextBoxes[count].setText("");
						
					}
						else 
							if(state.equals("Update"))
						{
							taVNumder.setEnabled(false);
						}

					}
				
				myTextBoxes[0].grabFocus();
				}
			}
			catch (Exception exc)
			{
				JOptionPane.showMessageDialog(this,"Error :"+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
			}
		}
//	***************************************************************************************
		void enableDisableButtons()
		{
			JButton[] buttons = {jbAdd,jbDelete,jbSearch,jbEdit};

			for(int count = 0;count<buttons.length;++count)
			{
				if(buttons[count].isEnabled())
				{
					buttons[count].setEnabled(false);
					jbCancel.setEnabled(true);
					jbSave.setEnabled(true);
				}
				else
				{
					buttons[count].setEnabled(true);
					jbCancel.setEnabled(false);
					jbSave.setEnabled(false);
					jbPrev.setEnabled(false);
					jbNext.setEnabled(true);
				}
			}
		}
//	****************************************************************************************
		public void valueChanged(ListSelectionEvent le)
		{
			try
			{
				dbc.connect();
				int number = Integer.parseInt(results.getSelectedValue().toString());

				sqlCommand = "SELECT * FROM Vehicle WHERE VehicleNo = ?";
				PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);
				prep.setInt(1,number);

				ResultSet rsSearch = prep.executeQuery();

				if(rsSearch.next())
				{
					taVNumder.setText(rsSearch.getString("VehicleNo"));
					taMake.setText(rsSearch.getString("Make"));
					taModel.setText(rsSearch.getString("Model"));
					taRegistration.setText(rsSearch.getString("Registration"));
					taRoute.setText(rsSearch.getString("Route"));
					
				}
				dbc.disconnect();
			}
			catch(NumberFormatException ex)
			{
				JOptionPane.showMessageDialog(this,"Please make sure you selected a number.","Vehicle Service",JOptionPane.OK_OPTION+JOptionPane.ERROR_MESSAGE);
			}
			catch(Exception exc)
			{
			}
		}
//	***************************************************************************************
		public class jbPrev_Click implements ActionListener
		{
			public void actionPerformed(ActionEvent ae)
			{
				try
				{
					if(rs.previous())
					{
						jbPrev.setEnabled(true);
						jbNext.setEnabled(true);
						taVNumder.setText(rs.getString("VehicleNo"));
						taMake.setText(rs.getString("Make"));
						taModel.setText(rs.getString("Model"));
						taRegistration.setText(rs.getString("Registration"));
						taRoute.setText(rs.getString("Route"));
						
					}
					else
					{
						jbPrev.setEnabled(false);
						jbNext.setEnabled(true);
					}
				}
				catch(Exception exc)
				{
					JOptionPane.showMessageDialog(null,"Error :"+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
				}
			}
		}
//	****************************************************************************************
		public class jbNext_Click implements ActionListener
		{
			public void actionPerformed(ActionEvent ae)
			{
				try
				{
					if(rs.next())
					{
						jbNext.setEnabled(true);
						jbPrev.setEnabled(true);
						taVNumder.setText(rs.getString("VehicleNo"));
						taMake.setText(rs.getString("Make"));
						taModel.setText(rs.getString("Model"));
						taRegistration.setText(rs.getString("Registration"));
						taRoute.setText(rs.getString("Route"));
					}
					else
					{
						jbNext.setEnabled(false);
						jbPrev.setEnabled(true);
					}
				}
				catch(Exception exc)
				{
					JOptionPane.showMessageDialog(null,"Error :"+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
				}
			}
		}
//	****************************************************************************************
		public class jbAdd_Click implements ActionListener
		{
			public void actionPerformed(ActionEvent ae)
			{
				state = "Add";

				//Enable & Disable Controls call
				enableDisableButtons();
				enableANDDisableTextboxes();

				jbNext.setEnabled(false);
				jbPrev.setEnabled(false);
			}
		}
//	****************************************************************************************
		public class jbDelete_Click implements ActionListener
		{
			public void actionPerformed(ActionEvent ae)
			{
				//Get value from textbox
				String vehinumber = taVNumder.getText();
				try
				{
					int decision = JOptionPane.showConfirmDialog(null,"Are you sure you want to delete the the vehicle ?","Vehicle Service",JOptionPane.YES_NO_OPTION);
					if(decision == 0)
					{
						//Delete Vehicle record
						sqlCommand = "DELETE FROM Vehicle WHERE VehicleNo = ?";
						PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);

						prep.setInt(1,Integer.parseInt(vehinumber));
						if (prep.executeUpdate() != 1)
						{
							throw new Exception();
						}
						JOptionPane.showMessageDialog(null,"Record deleted sucessfully","Vehicle Service",JOptionPane.OK_OPTION+JOptionPane.INFORMATION_MESSAGE);
						showFirstRecord();

						serviceGUI.cboVehicleNum.removeItem(vehinumber);
						reportServiceGUI.cboVNumber.removeItem(vehinumber);

						Vector clearVector = new Vector();
						results.setListData(clearVector);
					}
				}
				catch(Exception exc)
				{
					JOptionPane.showMessageDialog(null,"Error :"+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
				}
			}
		}
//	****************************************************************************************
		public class jbEdit_Click implements ActionListener
		{
			public void actionPerformed(ActionEvent ae)
			{
				state = "Update";

				//Enable & Disable Controls call
				enableDisableButtons();
				enableANDDisableTextboxes();

				jbNext.setEnabled(false);
				jbPrev.setEnabled(false);
			}
		}
//	****************************************************************************************
		public class jbSave_Click implements ActionListener
		{
			public void actionPerformed(ActionEvent ae)
			{
				//Get values from textBoxes
				String vehinumber = taVNumder.getText();
				String make = taMake.getText();
				String model = taModel.getText();
				String regis = taRegistration.getText();
				String route = taRoute.getText();
				boolean empty = validateInfo();

				if(empty == true)
				{
					JOptionPane.showMessageDialog(null,"Please enter all information.","Vehicle Service",JOptionPane.OK_OPTION+JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					if(state.equals("Add"))
					{
						try
						{
							dbc.connect();
							sqlCommand = "INSERT INTO Vehicle VALUES (?,?,?,?,?,?)";
							PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);

							prep.setInt(1,Integer.parseInt(vehinumber));
							prep.setString(2,make);
							prep.setString(3,model);
							prep.setString(4,regis);
							prep.setString(5,route);
							

							if(prep.executeUpdate() != 1)
							{
								throw new Exception("Insert not successful");
							}

     						//Insert service for new Vehicle  NB.Default values are zero's
							addVehicle();

							//Bring controls back to normal
							enableDisableButtons();
							enableANDDisableTextboxes();
						}
						catch(NumberFormatException ex)
						{
							JOptionPane.showMessageDialog(null,"Vehicle Number should be a number","Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
						}
						catch(Exception exc)
						{
							JOptionPane.showMessageDialog(null,"Error :"+ exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
						}
					}
					else if(state.equals("Update"))
					{
						try
						{
							dbc.connect();
							sqlCommand = "UPDATE Vehicle SET VehicleNo = ?,Make = ?,Model = ?,Registration = ?,Route = ?WHERE VehicleNo = 0";
							PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);

							prep.setInt(1,Integer.parseInt(vehinumber));
							prep.setString(2,make);
							prep.setString(3,model);
							prep.setString(4,regis);
							prep.setString(5,route);
							prep.setInt(6,Integer.parseInt(vehinumber));

							if(prep.executeUpdate() != 1)
							{
								throw new Exception("Update not successful");
							}
							JOptionPane.showMessageDialog(null,"Record updated sucessfully","Vehicle Service",JOptionPane.OK_OPTION+JOptionPane.INFORMATION_MESSAGE);

							//Bring controls back to normal
							enableDisableButtons();
							enableANDDisableTextboxes();
						}
						catch(Exception exc)
						{
							JOptionPane.showMessageDialog(null,"Error :"+ exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
						}
					}
				}
			}
		}
//	****************************************************************************************
		public class jbCancel_Click implements ActionListener
		{
			public void actionPerformed(ActionEvent ae)
			{
				enableANDDisableTextboxes();
				enableDisableButtons();
			}
		}
//	****************************************************************************************
		public class jbSearch_Click implements ActionListener
		{
			public void actionPerformed(ActionEvent ae)
			{
				searchVehicle app = new searchVehicle();
				app.showSearch();
			}
		}
	}
//	*****************************************************************************************
