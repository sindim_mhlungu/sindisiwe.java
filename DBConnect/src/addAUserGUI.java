import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
	class addAUserGUI extends JPanel implements ActionListener
	{
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		
		//Labels
		JLabel lblUser = new JLabel("User Name:");            
		JLabel lblPassword = new JLabel("Password:");
		
		//Textboxes
		JTextField taUser = new JTextField(10);              
		JPasswordField taPassword = new JPasswordField(10);
		
		//Buttons
		JButton jbAddUser = new JButton("Add User");
		
		//Variables
		String sqlCommand = "";                             
		DBConn dbc = new DBConn();
		
		void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
		{
			gbc.gridx = gx;
			gbc.gridy = gy;
			gbc.gridwidth = gw;
			gbc.gridheight = gh;
			gbc.weightx = wx;
			gbc.weighty = wy;
			gbc.fill = GridBagConstraints.BOTH;
			gbc.anchor = GridBagConstraints.EAST;
		}
//	****************************************************************************************
		addAUserGUI()
		{
			this.setLayout(gbl);
			layoutLabels();
			layoutTextboxes();
			layoutButtons();
			this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "User Management"));
			setVisible(true);
		}
//	****************************************************************************************
		void layoutLabels()
		{
			//lblUser
			buildConstraints(gbc,0,0,1,1,1,1);
			gbl.setConstraints(lblUser,gbc);
			this.add(lblUser);

			//lblPassword
			buildConstraints(gbc,0,1,1,1,1,1);
			gbl.setConstraints(lblPassword,gbc);
			this.add(lblPassword);
		}
//	****************************************************************************************
		void layoutTextboxes()
		{
			//taUser
			buildConstraints(gbc,1,0,1,1,1,0);
			gbl.setConstraints(taUser,gbc);
			this.add(taUser);

			//txtPassword
			buildConstraints(gbc,1,1,1,1,1,0);
			gbl.setConstraints(taPassword,gbc);
			this.add(taPassword);
			taPassword.setEchoChar('x');
		}
//	****************************************************************************************
		void layoutButtons()
		{
			//jbAddUser
			buildConstraints(gbc,0,2,2,1,1,1);
			gbl.setConstraints(jbAddUser,gbc);
			this.add(jbAddUser);
			jbAddUser.addActionListener(this);
			jbAddUser.setMnemonic('A');
		}
//	****************************************************************************************
		public void actionPerformed (ActionEvent ae)
		{
			if(ae.getSource() == jbAddUser)
			{
				if(taUser.getText().equals("")||taPassword.getPassword().toString().equals(""))
				{
					JOptionPane.showMessageDialog(null,"Please enter both UserName and Password","Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
				}
				else
				{
					try
					{
						dbc.connect();
						sqlCommand = "INSERT INTO UserManager VALUES (?,?)";
						PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);

						prep.setString(1,taUser.getText());
						prep.setString(2,taPassword.getPassword().toString());

						if(prep.executeUpdate() != 1)
						{
							throw new Exception("Update not successful");
						}
						JOptionPane.showMessageDialog(null,"User added sucessfully","Vehicle Service",JOptionPane.OK_OPTION+JOptionPane.INFORMATION_MESSAGE);
						dbc.disconnect();
					}
					catch(Exception exc)
					{
						System.err.println(exc.getMessage());
					}
				}

			}
		}
	}
//	*****************************************************************************************
