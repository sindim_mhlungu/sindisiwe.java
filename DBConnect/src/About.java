/*PROGRAMMER:Sindisiwe Mhlungu
  DUE DATE:21/10/2011
  TOPIC :learning Unit 12 to 16
  MODULE:PROGRAMMING_1B_Prog_6112
   
*/
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
import java.io.*;
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public class About extends JFrame implements ActionListener
{
                  JLabel jbAbout = new JLabel("ABOUT");
				  JTextArea jtaAbout = new JTextArea(8,5);
				  JButton jbClose = new JButton("CLOSE");
				  GridBagLayout gbl = new GridBagLayout();
				  JScrollPane jsp = new JScrollPane(jtaAbout);
				  GridBagConstraints gbc = new GridBagConstraints();
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
		{
		           gbc.gridx = gx;
				   gbc.gridy = gy;
				   
				   gbc.gridwidth = gw;
				   gbc.gridheight = gh;
				   
				   gbc.weightx = wx;
				   gbc.weighty = wy;
				   
				   gbc.anchor = GridBagConstraints.EAST;
				   gbc.fill = GridBagConstraints.BOTH;
                            }
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        About()
		{
		                               setSize(420,350);
				   setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				   setResizable(false);
				   JPanel panel_1 = new JPanel();
				   panel_1.setLayout(gbl);
				   
				   //center frame
				   Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
				   Dimension size = getSize();
				   
				   scrnSize.height = scrnSize.height/2;
				   scrnSize.width = scrnSize.width/2;
				   
				   size.height = size.height/2;
				   size.width = size.width/2;
				   
				   int x = scrnSize.width - size.width;
				   int y = scrnSize.height - size.height;
				   setLocation(x,y);
				   
				   //jbAbout
				   buildConstraints(gbc,0,0,1,1,1,1);
				   gbl.setConstraints(jbAbout,gbc);
				   gbc.anchor = GridBagConstraints.CENTER;
				   panel_1.add(jbAbout);
				   Font customFont = new Font("Comic Sans MS", Font.BOLD,16);
		                               jbAbout.setFont(customFont);
				   
				   
		           //jtaAbout
		           buildConstraints(gbc,0,1,2,1,1,1);
            	   gbl.setConstraints(jsp,gbc);
		           panel_1.add(jsp);
		           jtaAbout.setWrapStyleWord(true);
		           jtaAbout.setLineWrap(true);
		           jtaAbout.setEditable(false);

		           //jbClose
		           buildConstraints(gbc,1,2,1,1,1,1);
		           gbl.setConstraints(jbClose,gbc);
		           panel_1.add(jbClose);
		           jbClose.addActionListener(this);
				   
				   Image icon = Toolkit.getDefaultToolkit().getImage("Pics/home.gif");
				   this.setIconImage(icon);
				   
				   setContentPane(panel_1);
				   loadAboutText();
		}
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public	void loadAboutText()
    	
                  {
    		               jtaAbout.append("Developed for IIE 2011.");

    		               jtaAbout.append(" NB. Unauthorizedcopying of this software is illegal. ");
    	
    	                   jtaAbout.append("You will be contravening the intellectual property rights act if you make copies of this software.");
    	
                 }
		    
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public void actionPerformed(ActionEvent ae)
	        {
		            Object src = ae.getSource();
		            if(src == jbClose)
		        {
			        this.setVisible(false);
		        }
	        }
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    	public static void showAbout()
	        {
		            About app = new About();
		            app.setVisible(true);
	        }
}
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~		            
		            
				  
				   
		


