
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
import java.awt.geom.*;
public class splashVehicleScreen extends JFrame{
	JLabel jlHeader = new JLabel("Welcome to the Vehicle Service Tracking System",JLabel.CENTER);
	JLabel jlImage = new JLabel(new ImageIcon("Pics/vehicle.jpg"),JLabel.CENTER);
	JLabel jlFooter = new JLabel("Created by Code Mixers.inc",JLabel.CENTER);
	GridBagLayout gbl = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints();
//****************************************************************************************
	void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
	{
		gbc.gridx = gx;
		gbc.gridy = gy;
		
		gbc.gridwidth = gw;
		gbc.gridheight = gh;
		
		gbc.weightx = wx;
		gbc.weighty = wy;
		
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.BOTH;
	}
//****************************************************************************************
	splashVehicleScreen()
	{
		super("Loading...");
		setSize(450,300);
		setResizable(false);
		JPanel pane = new JPanel();
		pane.setLayout(gbl);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Center frame
		Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension size = getSize();
		scrnSize.height = scrnSize.height/2;
		scrnSize.width = scrnSize.width/2;
		size.height = size.height/2;
		size.width = size.width/2;
		
		int y = scrnSize.height - size.height;
		int x = scrnSize.width - size.width;
		setLocation(x, y);
		
		Image icon = Toolkit.getDefaultToolkit().getImage("Pics/home.gif");
		this.setIconImage(icon);
		Image myIcon = Toolkit.getDefaultToolkit().getImage("Pics/vehicle.gif");

		//lblHeader
		buildConstraints(gbc,1,0,1,1,1,2);
		gbl.setConstraints(jlHeader,gbc);
		pane.add(jlHeader);
		Font customFont = new Font("Comic Sans MS", Font.BOLD,20);
		jlHeader.setFont(customFont);

		//lblImage
		buildConstraints(gbc,1,1,1,1,1,2);
		gbl.setConstraints(jlImage,gbc);
		pane.add(jlImage);

		//lblFooter
		buildConstraints(gbc,1,2,1,1,1,2);
		gbl.setConstraints(jlFooter,gbc);
		pane.add(jlFooter);
		Font customFont1 = new Font("Comic Sans MS", Font.BOLD,20);
		jlFooter.setFont(customFont1);

		setContentPane(pane);
		//setVisible(true);
	}
//****************************************************************************************
	public void showSplash()throws Exception
	{
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		setVisible(true);
	}
}
//*****************************************************************************************
