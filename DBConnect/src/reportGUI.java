
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
//*****************************************************************************************
class reportServiceGUI extends JPanel
{
	GridBagLayout gbl = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints();
	//Labels
	JLabel lblVehicleNumber = new JLabel("VehicleNo:");       
	JLabel lblRegistrationLabel = new JLabel("Registration:");
	JLabel lblRegistration = new JLabel("    ");
	
	//ComboBox
	public static JComboBox cboVNumber = new JComboBox();
	
	//Buttons
	JButton jbPrintService = new JButton("Print Service Report");
	
	//Table
	JTable tblReportTable = new JTable(20,6);
	JScrollPane jsp = new JScrollPane(tblReportTable);
	
	//Variables
	DBConn dbc = new DBConn();                                
	String sqlCommand;
	int loopCount = 0;
	int rowsForRate = 0;
//*****************************************************************************************
	void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
	{
		gbc.gridx = gx;
		gbc.gridy = gy;
		
		gbc.gridwidth = gw;
		gbc.gridheight = gh;
		
		gbc.weightx = wx;
		gbc.weighty = wy;
		
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.BOTH;
	}
//*****************************************************************************************
	reportServiceGUI()
	{
		this.setLayout(gbl);
		layoutTable();
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Report Management"));
		setVisible(true);
		//Load the combo with Student Numbers
		loadCombo();
		// get infor from database
		getImformatiom();
		//Get the Classe's avarage
		//getCostAvarage();
	}
//*****************************************************************************************
	public void layoutTable()
	{
		//lblVehicleNumber
		buildConstraints(gbc,0,0,1,1,1,0);
		gbl.setConstraints(lblVehicleNumber,gbc);
		this.add(lblVehicleNumber);
		
		//lblRegistrationLabel
		buildConstraints(gbc,0,1,1,1,0,0);
		gbl.setConstraints(lblRegistrationLabel,gbc);
		this.add(lblRegistrationLabel);
		
		//lblRegistration
		buildConstraints(gbc,1,1,1,1,0,0);
		gbl.setConstraints(lblRegistration,gbc);
		this.add(lblRegistration);
		Font customFont = new Font("Times New Roman",Font.BOLD,12);
		lblRegistration.setFont(customFont);
		
		//cboVNumber
		buildConstraints(gbc,1,0,1,1,1,0);
		gbl.setConstraints(cboVNumber,gbc);
		this.add(cboVNumber);
		cboVNumber.addItemListener(new cboVNumber_Click());
		
		//tblReportTable
		buildConstraints(gbc,0,3,3,1,1,10);
		gbl.setConstraints(jsp,gbc);
		this.add(jsp);
		tblReportTable.setGridColor(Color.blue);
		tblReportTable.setEnabled(false);
		tblReportTable.setValueAt("Service Name",0,0);
		tblReportTable.setValueAt("Service cost/km",0,1);
		tblReportTable.setValueAt("Avarage cost per km" , 6,0);
		tblReportTable.setShowHorizontalLines(false);
		tblReportTable.setAutoCreateColumnsFromModel(true);
		tblReportTable.sizeColumnsToFit(2000);
	}
//*****************************************************************************************
	public void loadCombo()
	{
		try
		{
			dbc.connect();
			sqlCommand = "SELECT * FROM Vehicle";
			ResultSet rs = dbc.select(sqlCommand);
			while(rs.next())
			{
				String vehinumber = rs.getString("VehicleNo");
				cboVNumber.addItem(vehinumber);
			}
			dbc.disconnect();
		}
		catch(Exception exc)
		{
			System.err.println(exc.getMessage());
		}
	}
//*****************************************************************************************
	/*public void loadService()
	{
		try
		{
			int  rowsForOther = 0 ;
			
			
			int VehicleNo = Integer.parseInt(cboVNumber.getSelectedItem().toString());
			dbc.connect();
			sqlCommand = "SELECT * FROM Service";
			ResultSet rs = dbc.select(sqlCommand);
			while(rs.next())
			{
				sqlCommand = "SELECT * FROM Service s,ServiceTpye st, WHERE  s.ServiceTypeID = st.ServiceTypeID AND st.ServiceName = ? AND s.CostPerKM = ?";
				PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);
 
				
				prep.setInt(1,VehicleNo);

				ResultSet rs1 = prep.executeQuery();
				
				
				while(rs1.next())
				{
					double CostPerKM = rs1.getInt("CostPerKM");
					tblReportTable.setValueAt(String.valueOf(CostPerKM),rs1.getRow(),1);
					tblReportTable.setValueAt(rs1.getString("ServiceName"),rs1.getRow(),0);
					//tblReportTable.setValueAt(rs1.getString("Service cost/km"),rs1.getRow(),2);
					
					
					
					double	sericeType = rs1.getInt("SericeTypeID");
					sericeType ++;
					
					
					
				
				}
			}

			rowsForRate = rowsForOther +1;
			//Disconnect from DB
			dbc.disconnect();
		}
		catch(Exception exc)
		{
			
			System.err.println(exc.getMessage());
		}
	}*/
//*****************************************************************************************
/*	public void getCostPerKM()
	{
		try
		{ 
			dbc.connect();
			sqlCommand = "SELECT * FROM Service";
			PreparedStatement pst = dbc.con.prepareStatement(sqlCommand,ResultSet.TYPE_SCROOL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		}
		
	}*/
//*****************************************************************************************************	
	public void getImformatiom()
	{
		try
		{ 
			int vehicleNum = 0;
			double avg = 0;
		     dbc.connect();
		
		     sqlCommand = "SELECT * FROM Service";
		     ResultSet rs = dbc.select(sqlCommand);
		 
		  while(rs.next())
		  {
			  vehicleNum = rs.getInt("VehicleNo");
			  
			  sqlCommand = "SELECT * FROM Service WHERE VehicleNo = 2";
			  PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);
			  
			 prep.setInt(1,vehicleNum);
			  ResultSet rs1 = prep.executeQuery();
			  
			  while (rs1.next())
			  {
				  avg  =  rs1.getInt("CostPerKM");
				  avg = avg  * 100;
				  tblReportTable.setValueAt(avg,loopCount ,1);
			  }
			  tblReportTable.setValueAt(String.valueOf(avg),rs1.getRow(),1);
				dbc.disconnect();
			}
		}
			catch(Exception exc)
			{
				System.err.println(exc.getMessage());
			}
		}
//*****************************************************************************************
	/*public void getCostAvarage()
	{
		try
		{
			dbc.connect();
			double avg = 0;
			double totCostPerKM = 0 ; int  sericeType = 0;

			sqlCommand = "SELECT * FROM Service";
			ResultSet rs = dbc.select(sqlCommand);

			while(rs.next())
			{
				
				avg = rs.getInt("Cost Avarage");
				 avg  = totCostPerKM / sericeType;
			}
			tblReportTable.setValueAt(String.valueOf(avg),rowsForRate + 3,1);

			dbc.disconnect();
		}
		catch(Exception exc)
		{
			System.err.println(exc.getMessage());
		}
	}*/
//*****************************************************************************************
	public class cboVNumber_Click implements ItemListener
	{
		public void itemStateChanged (ItemEvent ie)
		{
			try
			{
				dbc.connect();

				String regisNum;
				int vehiNum = Integer.parseInt(cboVNumber.getSelectedItem().toString());
				sqlCommand = "SELECT * FROM Vehicle  WHERE VehicleNo = ?";
				PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);

				prep.setInt(1,vehiNum);
				ResultSet rs = prep.executeQuery();
				while(rs.next())
				{
					regisNum = rs.getString("Registration");
					lblRegistration.setText(regisNum);
				}
				dbc.disconnect();

				//loadService();
				getImformatiom();
			}
			catch(Exception exc)
			{
				System.err.println(exc.getMessage());
			}
		}
	}
}
//*****************************************************************************************