import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
import java.io.*;
public class helpTopics extends JFrame implements ActionListener
{
	JTextArea txtHelp = new JTextArea(8,8);          
	JLabel jlHeader = new JLabel("Help Topics:");
	JButton jbClose = new JButton("Close");
	GridBagLayout gbl = new GridBagLayout();         
	JScrollPane jsp = new JScrollPane(txtHelp);
	GridBagConstraints gbc = new GridBagConstraints();
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
	{
		gbc.gridx = gx;
		gbc.gridy = gy;
		gbc.gridwidth = gw;
		gbc.gridheight = gh;
		gbc.weightx = wx;
		gbc.weighty = wy;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.BOTH;
	}
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	helpTopics()
	{
		super("Help Topics");
		setSize(420,350);
		setResizable(false);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(gbl);

		//Center frame
		Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension size = getSize();
		
		scrnSize.height = scrnSize.height/2;
		scrnSize.width = scrnSize.width/2;
		
		size.height = size.height/2;
		size.width = size.width/2;
		
		int y = scrnSize.height - size.height;
		int x = scrnSize.width - size.width;
		setLocation(x, y);

		Image icon = Toolkit.getDefaultToolkit().getImage("Pics/home.gif");
		this.setIconImage(icon);

		//jlHeader
		buildConstraints(gbc,0,0,1,1,1,1);
		gbl.setConstraints(jlHeader,gbc);
		panel_1.add(jlHeader);
		Font customFont = new Font("Comic Sans MS", Font.BOLD,14);
		jlHeader.setFont(customFont);

		//jtaHelp
		buildConstraints(gbc,0,1,2,1,1,1);
		gbl.setConstraints(jsp,gbc);
		panel_1.add(jsp);
		txtHelp.setLineWrap(true);
		txtHelp.setWrapStyleWord(true);
		txtHelp.setEditable(false);

		//jbClose
		buildConstraints(gbc,1,2,1,1,1,0);
		gbl.setConstraints(jbClose,gbc);
		jbClose.setMnemonic('C');
		panel_1.add(jbClose);
		jbClose.addActionListener(this);
		jbClose.setMnemonic('C');

		setContentPane(panel_1);

		loadText();
	}
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	void loadText()
	{
		try
		{
			FileReader inStream = new FileReader("HelpTopics.txt");
			StringBuffer sbr = new StringBuffer("");
			int count = 0;
			while((count = inStream.read()) != -1)
			{
				sbr.append((char)count);
			}
			txtHelp.append(sbr.toString());
		}
		catch(Exception exc)
		{
			System.err.println(exc.getMessage());
		}
	}
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public void actionPerformed(ActionEvent ae)
	{
		Object src = ae.getSource();
		if(src == jbClose)
		{
			setVisible(false);
		}
	}
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public static void showHelp()
	{
		helpTopics app = new helpTopics();
		app.setVisible(true);
	}
}
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~