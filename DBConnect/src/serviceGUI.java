import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
class serviceGUI extends JPanel implements ActionListener
{
	GridBagLayout gbl = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints();
	//JDropdown Lists
	static  JComboBox cboVehicleNum = new JComboBox();    
	static  JComboBox cboServiceType = new JComboBox();
	//JLabels
	JLabel jlVehicleNum = new JLabel("Vehicle Number:");     
	JLabel jlRegistration = new JLabel("Registration:");   
	JLabel jlCurrent = new JLabel("Current odo:");        
	JLabel jlLast = new JLabel("Last odo:");    
	JLabel jlServiceT = new JLabel("Service type:");
    JLabel jlCost = new JLabel("Cost:");
	JLabel jlCostPerKM = new JLabel("Cost per KM:");
	//JTextFields
	JTextField taVehicleNum = new JTextField(3);        
	JTextField taRegistration = new JTextField(3);        
	JTextField taCurrent = new JTextField(3);       
	JTextField taLast = new JTextField(3);
	JTextField taServiceT = new JTextField(3); 
    JTextField taCost = new JTextField(3);
	JTextField taCostPerKM = new JTextField(3);
	//JButtons
	JButton jbCancel = new JButton("Cancel");      JButton jbCalculate = new JButton("Calculate cost per KM");
	JButton jbEdit = new JButton("Edit Cost");  JButton jbSave = new JButton("Save");
	//Variables
	DBConn dbc = new DBConn();
	String sqlCommand;
//*****************************************************************************************
	void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
	{
		gbc.gridx = gx;
		gbc.gridy = gy;
		gbc.gridwidth = gw;
		gbc.gridheight = gh;
		gbc.weightx = wx;
		gbc.weighty = wy;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.BOTH;
	}
//*****************************************************************************************
	serviceGUI()
	{
		this.setLayout(gbl);
		layoutLabels();
		layoutTextboxes();
		layoutButtons();
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Vehicle Service Information"));
		setVisible(true);
		//Populate Vehicle Number combobox with Vehicle Numbers
		loadCombo();
		//Populate Subject combobox with subject names
		loadServiceType();
	}
//******//***********************************************************************************
	void layoutLabels()
	{
		//jlVehicleNum
		buildConstraints(gbc,0,0,1,1,1,1);
		gbl.setConstraints(jlVehicleNum,gbc);
		this.add(jlVehicleNum);
		
		//jlRegistration
		buildConstraints(gbc,0,1,1,1,1,1);
		gbl.setConstraints(jlRegistration,gbc);
		this.add(jlRegistration);
		
		// jlCurrent
		buildConstraints(gbc,0,2,1,1,1,1);
		gbl.setConstraints( jlCurrent,gbc);
		this.add( jlCurrent);
		
		//jlLast
		buildConstraints(gbc,0,3,1,1,1,1);
		gbl.setConstraints(jlLast,gbc);
		this.add(jlLast);
		
		//jlServiceT
		buildConstraints(gbc,0,4,1,1,1,1);
		gbl.setConstraints(jlServiceT,gbc);
		this.add(jlServiceT);
		
		//jlCost
		buildConstraints(gbc,0,5,1,1,1,1);
		gbl.setConstraints(jlCost,gbc);
		this.add(jlCost);
		
		//jlCostPerKM
		buildConstraints(gbc,0,6,1,1,1,1);
		gbl.setConstraints(jlCostPerKM,gbc);
		this.add(jlCostPerKM);
		Font myFont = new Font("Times New Roman",Font.BOLD,14);
		jlCostPerKM.setFont(myFont);
	}
//*****************************************************************************************
	void layoutTextboxes()
	{
		//cboVehicleNum
		buildConstraints(gbc,1,0,1,1,1,0);
		gbl.setConstraints(cboVehicleNum,gbc);
		this.add(cboVehicleNum);
		cboVehicleNum.addItemListener(new cboVehicleNum_Click());
		//cboVehicleNum.addActionListener(this);
		
		//taRegistartion
		buildConstraints(gbc,1,1,1,1,1,0);
		gbl.setConstraints(taRegistration,gbc);
		this.add(taRegistration);
		taRegistration.setEnabled(false);
		
		//taCurrent
		buildConstraints(gbc,1,2,1,1,1,0);
		gbl.setConstraints(taCurrent,gbc);
		this.add(taCurrent);
		taCurrent.setEnabled(false);
		
		//taLast
		buildConstraints(gbc,1,3,1,1,1,0);
		gbl.setConstraints(taLast,gbc);
		this.add(taLast);
		taLast.setEnabled(false);
		
		//cboServiceT
		buildConstraints(gbc,1,4,1,1,1,0);
		gbl.setConstraints(cboServiceType,gbc);
		this.add(cboServiceType);
		cboServiceType.addItemListener(new cboServiceType_Click());
		
	    //taCost
		buildConstraints(gbc,1,5,1,1,1,0);
		gbl.setConstraints(taCost,gbc);
		this.add(taCost);
		taCost.setEnabled(false);
		
		//taCostPerKM
		buildConstraints(gbc,1,6,1,1,1,0);
		gbl.setConstraints(taCostPerKM,gbc);
		this.add(taCostPerKM);
		taCostPerKM.setEnabled(false);
	}
//*****************************************************************************************
	void layoutButtons()
	{
		//jbCalculate
		buildConstraints(gbc,0,9,1,1,1,1);
		gbl.setConstraints(jbCalculate,gbc);
		this.add(jbCalculate);
		jbCalculate.setMnemonic('l');
		jbCalculate.addActionListener(new calculate_Click());
		jbCalculate.setEnabled(false);
		
		//jbEdit
		buildConstraints(gbc,1,9,1,1,1,1);
		gbl.setConstraints(jbEdit,gbc);
		this.add(jbEdit);
		jbEdit.setMnemonic('E');
		jbEdit.addActionListener(new jbEdit_Click());
		
		//jbSave
		buildConstraints(gbc,1,10,1,1,1,1);
		gbl.setConstraints(jbSave,gbc);
		this.add(jbSave);
		jbSave.setMnemonic('S');
		jbSave.addActionListener(new jbSave_Click());
		jbSave.setEnabled(false);
		
		//jbCancel
		buildConstraints(gbc,0,10,1,1,1,1);
		gbl.setConstraints(jbCancel,gbc);
		this.add(jbCancel);
		jbCancel.setMnemonic('C');
		jbCancel.addActionListener(new jbCancel_Click());
		jbCancel.setEnabled(false);
	}
//*****************************************************************************************
	void enableANDDisableControls()
	{
		JTextField[] myTextBoxes = {taRegistration,taCurrent,taLast,taCost,taCostPerKM};
		JButton[] myButtons = {jbEdit,jbCalculate,jbCancel,jbSave};
		for(int i=0;i<myButtons.length;++i)
		{
			if(myButtons[i].isEnabled() == true)
			{
				myButtons[i].setEnabled(false);
				jbEdit.setEnabled(true);
			}
			else
			{
				myButtons[i].setEnabled(true);
				jbEdit.setEnabled(false);
			}
		}
		for(int i1=0;i1<myTextBoxes.length;++i1)
		{
			if(myTextBoxes[i1].isEnabled() == true)
			{
				myTextBoxes[i1].setEnabled(false);
			}
			else
			{
				myTextBoxes[i1].setEnabled(true);
			}
		}
	}
//*****************************************************************************************
	public void loadCombo()
	{
		try
		{
			dbc.connect();

			sqlCommand = "SELECT * FROM Vehicle";
			ResultSet rs = dbc.select(sqlCommand);

			while (rs.next())
			{
				cboVehicleNum.addItem(rs.getString("VehicleNO"));
			}
			dbc.disconnect();
		}
		catch(Exception exc)
		{
			JOptionPane.showMessageDialog(this,"Error:"+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
		}
	}
//*****************************************************************************************
	void loadServiceType()
	{
		try
		{
			dbc.connect();

			sqlCommand = "SELECT * FROM ServiceType";
			ResultSet rs = dbc.select(sqlCommand);

			while (rs.next())
			{
				cboServiceType.addItem(rs.getString("ServiceName"));
			}
			dbc.disconnect();
		}
		catch(Exception exc)
		{
			JOptionPane.showMessageDialog(this,"Error:"+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
		}
	}
//*****************************************************************************************
	public int getServiceID()
	{
		int servID = 0;
		String service = cboServiceType.getSelectedItem().toString();
		try
		{
			dbc.connect();
			//Get the ServiceID through the Service Name
			sqlCommand = "SELECT * FROM Service WHERE VehicleNo = ?";
			PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);
			prep.setString(1,service);

			ResultSet rs = prep.executeQuery();

			if (rs.next())
			{
				servID = rs.getInt(1);
			}
		}
		catch(Exception exc)
		{

		}
		return servID;
	}
//*****************************************************************************************
	void loadVehicleInfor()
	{
		String number = cboVehicleNum.getSelectedItem().toString();
		int servID = getServiceID();
		try
		{
			dbc.connect();
			sqlCommand = "SELECT * FROM Service s,Vehicle v,ServiceType st  WHERE  v.VehicleNo = s.VehicleNo AND s.ServiceTypeID = st.ServiceTypeID AND v.VehicleNo =" + number;
			
			ResultSet rs = dbc.select(sqlCommand);

			if(rs.next())
			{
				taVehicleNum .setText(rs.getString("VehicleNo"));
				taRegistration.setText(rs.getString("Registration"));
				taCurrent.setText(rs.getString("CurrentOdo"));
				taLast.setText(rs.getString("LastOdo"));
				taCost.setText(rs.getString("Cost"));
				taCostPerKM.setText(rs.getString("CostPerKM"));
			}
			dbc.disconnect();
		}
		catch(Exception exc)
		{
			JOptionPane.showMessageDialog(null,"Error: "+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
		}
	}
//*****************************************************************************************
	public class cboVehicleNum_Click implements ItemListener
	{
		public void itemStateChanged(ItemEvent ie)
		{
			String number = cboVehicleNum.getSelectedItem().toString();
			System.out.println("number = " + number);
			try
			{
				dbc.connect();

				sqlCommand = "SELECT * FROM Vehicle WHERE VehicleNo = " + number;
				
				ResultSet rs = dbc.select(sqlCommand);
				
				if (rs.next())
				{
					taVehicleNum .setText(rs.getString("VehicleNo"));
					taRegistration.setText(rs.getString("Registration"));
					taCurrent.setText(rs.getString("CurrentOdo"));
					
					
				}
				dbc.disconnect();
				loadVehicleInfor();
			}
			catch(Exception exc)
			{
			}
		}
	}
//*****************************************************************************************
	public class jbCancel_Click implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			enableANDDisableControls();
			loadVehicleInfor();
		}
	}
//*****************************************************************************************
	public class cboServiceType_Click implements ItemListener
	{
		public void itemStateChanged(ItemEvent ie)
		{
			loadVehicleInfor();
		}
	}
//*****************************************************************************************
	public class calculate_Click implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			try
			{
				int CurrentOdo = Integer.parseInt(taCurrent.getText());
				int LastOdo = Integer.parseInt(taLast.getText());
				double Cost = Integer.parseInt(taCost.getText());
				double CostPerKM;

			
				CostPerKM = (Cost/(CurrentOdo - LastOdo) )* 1.14;
				taCostPerKM.setText(Double.toString(CostPerKM));
			 
				
					
			}
			catch(NumberFormatException exc)
			{
				JOptionPane.showMessageDialog(null,"Please make sure all required information is entered","Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
			}
		}
	}
//*****************************************************************************************
	public class jbEdit_Click implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			taCostPerKM.setText("0");
			enableANDDisableControls();
		}
	}
//*****************************************************************************************
	public void ActionPerformed(ActionEvent e)
	{	
		 taVehicleNum.setText(cboVehicleNum.getSelectedItem().toString());
	}
	
	//*****************************************************************************************
	public class jbSave_Click implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			try
			{
				int servID = getServiceID();
				int VehicleNo = Integer.parseInt(cboVehicleNum.getSelectedItem().toString());
				String service = cboServiceType.getSelectedItem().toString();
				int CurrentOdo = Integer.parseInt(taCurrent.getText());
				int LastOdo = Integer.parseInt(taLast.getText());
				double CostPerKM = Double.parseDouble(taCostPerKM.getText());
				if (CostPerKM == 0)
				{
					JOptionPane.showMessageDialog(null,"Click 'Calculate cost per KM' button before attempting to save.","Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
				}
				else
				{
					try
					{
						dbc.connect();

						sqlCommand = "UPDATE Service SET CurrentOdo = ?,LastOdo = ?,CostPerKM = ? WHERE VehicleNo = ? AND ServiceTypeID = ?";
						PreparedStatement prep = dbc.con.prepareStatement(sqlCommand);

						prep.setInt(1,VehicleNo);
						prep.setInt(2,CurrentOdo);
						prep.setInt(3,LastOdo);
						prep.setString(4,service);
						prep.setDouble(5,CostPerKM);
						prep.setInt(6,servID);

						if(prep.executeUpdate() != 1)
						{
							throw new Exception("Update not successful");
						}
						JOptionPane.showMessageDialog(null,"Record updated successfully.","Vehicle Service",JOptionPane.INFORMATION_MESSAGE + JOptionPane.OK_OPTION);
						dbc.disconnect();
						enableANDDisableControls();
					}
					catch(Exception exc)
					{
						JOptionPane.showMessageDialog(null,"Error: "+exc.getMessage(),"Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
					}
				}
			}
			catch(NumberFormatException exc)
			{
				JOptionPane.showMessageDialog(null,"Please make sure all required information is entered","Vehicle Service",JOptionPane.ERROR_MESSAGE + JOptionPane.OK_OPTION);
				taCostPerKM.setText("0");
			}
		}
	}
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
//*****************************************************************************************
