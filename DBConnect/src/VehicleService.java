    import java.awt.*;
	import java.awt.event.*;
	import javax.swing.*;
	import javax.swing.event.*;
	import java.util.*;
	public class VehicleService extends JFrame implements ActionListener{
		JTabbedPane tebMain = new JTabbedPane();
		JButton jbExit = new JButton("Exit");
		//Toolbar
		JToolBar toolBar = new JToolBar();
		String iconPics[] = {"Pics/user.gif","Pics/report.gif","Pics/Exit.gif"};
		String buttonLabels[] = {"New User","View Report","Exit Application"};
		ImageIcon[] icons = new ImageIcon[iconPics.length];
		JButton[] buttons = new JButton[buttonLabels.length];
		JMenuBar menuB = new JMenuBar();

		JMenu file = new JMenu("File");

		JMenuItem fileVehicleDetails = new JMenuItem("Vehicle Details");
		JMenuItem fileReport = new JMenuItem("View Report");
		JMenuItem fileService = new JMenuItem("Vehicle Service");
		JMenuItem fileAddUser = new JMenuItem("Add new User");
		JMenuItem fileExit = new JMenuItem("Exit");

		JMenu Help = new JMenu("Help");

		JMenuItem helpTopics = new JMenuItem("Help Topics");
		JMenuItem helpAbout = new JMenuItem("About");
//	****************************************************************************************
		void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
		{
			gbc.gridx = gx;
			gbc.gridy = gy;
			
			gbc.gridwidth = gw;
			gbc.gridheight = gh;
			
			gbc.weightx = wx;
			gbc.weighty = wy;
			gbc.fill = GridBagConstraints.BOTH;
		}
//	****************************************************************************************
		VehicleService()
		{
			super("Vehicle Service");
			setSize(700,500);
			addWindowListener(new WindowHandler());

			JPanel panel_1 = new JPanel();
			GridBagLayout gbl = new GridBagLayout();
			GridBagConstraints gbc = new GridBagConstraints();
			panel_1.setLayout(gbl);

			//ActionListeners for menu
			menuB.add(file);
			menuB.add(Help);
			file.setMnemonic('F');
			file.add(fileVehicleDetails);                                
			file.add(fileReport);
			file.add(fileService);  
			fileVehicleDetails.addActionListener(this);                      
			fileReport.addActionListener(this);
		    fileService.addActionListener(this);

			file.add(fileAddUser);
			file.addSeparator();
			file.add(fileExit);
			fileExit.addActionListener(this);
			fileAddUser.addActionListener(this);

			Help.setMnemonic('H');
			Help.add(helpTopics);                                 
			helpTopics.addActionListener(this);
			Help.add(helpAbout);                                   
			helpAbout.addActionListener(this);

			setJMenuBar(menuB);

			//Set the icon for the frame
			Image icon = Toolkit.getDefaultToolkit().getImage("Pics/home.gif");
			this.setIconImage(icon);

			tebMain.addTab("Vehicle",new VehicleGUI());
			tebMain.addTab("Services",new serviceGUI());
			tebMain.addTab("Services Report",new reportServiceGUI());
			tebMain.addTab("Add a User",new addAUserGUI());
			tebMain.setBackground(Color.white);

			buildConstraints(gbc,0,1,1,1,10,20);
			gbc.anchor = GridBagConstraints.EAST ;
			gbl.setConstraints(tebMain,gbc);
			panel_1.add(tebMain);

			tebMain.setToolTipTextAt(0,"Click to see vehicle info.");
			tebMain.setToolTipTextAt(1,"Click to see services details");
			tebMain.setToolTipTextAt(2,"Click to view  service report");
			tebMain.setToolTipTextAt(3,"Click to add a user");

			buildConstraints(gbc,0,2,1,1,1,1);
			gbc.anchor = GridBagConstraints.EAST ;
			gbl.setConstraints(jbExit,gbc);
			panel_1.add(jbExit);
			jbExit.setMnemonic('x');
			jbExit.addActionListener(this);

			for(int i=0;i<buttonLabels.length;++i)
			{
				icons[i] = new ImageIcon(iconPics[i]);
				buttons[i] = new JButton(icons[i]);
				buttons[i].setToolTipText(buttonLabels[i]);

				buttons[i].addActionListener(this);
				if(i==3) toolBar.addSeparator();
				toolBar.add(buttons[i]);
			}
			//Toolbar
			buildConstraints(gbc,0,0,1,1,1,1);
			gbc.anchor = GridBagConstraints.EAST ;
			gbl.setConstraints(toolBar,gbc);
			panel_1.add(toolBar);

			//Content Pane
			setContentPane(panel_1);
			setVisible(true);
		}
//	****************************************************************************************
		public void actionPerformed(ActionEvent ae)
		{
			Object src = ae.getSource();
			if(src == jbExit || src == fileExit || src == buttons[2])
			{
				exitDecision();
			}
			else if(src == fileAddUser || src == buttons[0])
			{
				tebMain.setSelectedIndex(3);
			}
			else if(src == fileVehicleDetails)
			{
				tebMain.setSelectedIndex(0);
			}
			else if(src == fileReport || src == buttons[1])
			{
				tebMain.setSelectedIndex(2);
			}
			else if(src == fileService)
			{
				tebMain.setSelectedIndex(1);
			}
			else if(src == helpTopics)
			{
				helpTopics het = new helpTopics();
				het.showHelp();
			}
			else if(src == helpAbout)
			{
				About abt = new About();
				abt.showAbout();
			}
		}
//	****************************************************************************************
		public class WindowHandler extends WindowAdapter
		{
			public void windowClosing(WindowEvent we)
			{
				exitDecision();
			}
		}
//	****************************************************************************************
		public int exitDecision()
		{
			int decide = JOptionPane.showConfirmDialog(this,"Are you sure you want to exit ?","Vehicle Records",JOptionPane.YES_NO_OPTION);
			if(decide == 0)
			{
				System.exit(0);
			}
			else
			{
				setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			}
			return -1;
		}
//	****************************************************************************************

		static void instantiateAndCenterFrame()
		{
			//Instantiate Class
			VehicleService app = new VehicleService();
			app.setVisible(true);

			//Center frame on screen
			Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension frameSize = app.getSize();
			if (frameSize.height > scrnSize.height) {
				frameSize.height = scrnSize.height;
			}
			if (frameSize.width > scrnSize.width) {
				frameSize.width = scrnSize.width;
			}
			app.setLocation((scrnSize.width - frameSize.width) / 2, (scrnSize.height - frameSize.height) / 2);
		}
//	****************************************************************************************
		public static void showMain() throws Exception
		{
			//Make look and feel like this systems'
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			//Call method to show form
			instantiateAndCenterFrame();
		}
	}
//	*****************************************************************************************
