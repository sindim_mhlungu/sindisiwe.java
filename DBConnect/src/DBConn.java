/**
 * 
 * @author Sindisiwe
 *
 */
import java.sql.*;
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public class DBConn
{
			/**
			 * Method for database connection to Microsoft Access Database
			 */
            Connection con;
			public void connect()
			{ 
			        try
					{
					
      		        	//dsn less connection (direct connection)
			     	    Class.forName ("sun.jdbc.odbc.JdbcOdbcDriver");
			     	    String myDB = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=VehicleDB.mdb";
			     	    con = DriverManager.getConnection (myDB, "", "");
					}
					catch(Exception ex)
					{
					        System.out.println("Problem with connection " + ex.getMessage());
					}
		    }				
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			/**
			 * Method for database disconnection 
			 */
    	    public void disconnect()
	        {
		        try
		        {
		        	//Closing connection to DB
			            con.close();
		        }
		        catch(Exception ex)
		        {
			            System.err.println(ex.getMessage());
		        }
	        }
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    	    /**
			 * Method for SQL creating connection statement 
			 */
            public ResultSet select(String sql)
	        {
		        try
		        {
		        	
		        	//for creating connecting statement
			            Statement st = con.createStatement();
			            return st.executeQuery(sql);

		        }
		        catch(Exception ex)
		        {
			            System.err.println();
		        }
		        return null;
	        }
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            /**
			 * Method for Updating 
			 */
            public int change(String sql)
	        {
		        try
		        {
			            Statement st = con.createStatement();
			            return st.executeUpdate(sql);
		        }
		        catch(Exception ex)
		        {
			            System.err.println();
		        }
		        return -1;
	        }
}
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~