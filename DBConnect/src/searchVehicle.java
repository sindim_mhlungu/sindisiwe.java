import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
public class searchVehicle extends JFrame implements ActionListener
{
	GridBagLayout gbl = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints();
	
	//Labels
	JLabel lblCategory = new JLabel("Category to search by:");      
	JLabel lblSearchFor = new JLabel("Search for:");
	
	//Combobox
	public static JComboBox cboCategory = new JComboBox();
	
	//Textbox
	public static JTextField taSearchFor = new JTextField(10);
	
	//Buttons
	JButton jbSearch = new JButton("Search");                      
	JButton jbCancel = new JButton("Cancel");
	DBConn dbc = new DBConn();
	String myCategory,sqlCommand;
//	 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
	{
		gbc.gridx = gx;
		gbc.gridy = gy;
		
		gbc.gridwidth = gw;
		gbc.gridheight = gh;
		
		gbc.weightx = wx;
		gbc.weighty = wy;
		
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.BOTH;
	}
//	 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	searchVehicle()
	{
		super("Search for vehicle");
		setSize(300,100);

		JPanel pane = new JPanel();
		pane.setLayout(gbl);
		setResizable(false);
		
		//Center frame
		Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension size = getSize();
		
		scrnSize.height = scrnSize.height/2;
		scrnSize.width = scrnSize.width/2;
		
		size.height = size.height/2;
		size.width = size.width/2;
		
		int y = scrnSize.height - size.height;
		int x = scrnSize.width - size.width;
		setLocation(x, y);

		//Set the icon for the frame
		Image icon = Toolkit.getDefaultToolkit().getImage("Pics/home.gif");
		this.setIconImage(icon);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		//lblCategory
		buildConstraints(gbc,0,0,1,1,1,0);
		gbl.setConstraints(lblCategory,gbc);
		pane.add(lblCategory);

		//cboCategory
		buildConstraints(gbc,1,0,1,1,1,0);
		gbl.setConstraints(cboCategory,gbc);
		pane.add(cboCategory);
		cboCategory.addActionListener(this);

		//lblSearchFor
		buildConstraints(gbc,0,1,1,1,1,0);
		gbl.setConstraints(lblSearchFor,gbc);
		pane.add(lblSearchFor);

		//taSearchFor
		buildConstraints(gbc,1,1,1,1,1,0);
		gbl.setConstraints(taSearchFor,gbc);
		pane.add(taSearchFor);

		//jbSearch
		buildConstraints(gbc,0,2,1,1,0,1);
		gbl.setConstraints(jbSearch,gbc);
		pane.add(jbSearch);
		jbSearch.setMnemonic('S');
		jbSearch.addActionListener(this);

		//jbSearch
		buildConstraints(gbc,1,2,1,1,0,1);
		gbl.setConstraints(jbCancel,gbc);
		pane.add(jbCancel);
		jbCancel.setMnemonic('C');
	    jbCancel.addActionListener(this);

		setContentPane(pane);
	}
//	 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public void showSearch()
	{
		setVisible(true);
		cboCategory.removeAllItems();
		cboCategory.addItem("VehicleNo");
		cboCategory.addItem("Registration");
		taSearchFor.setText("");
	}
//	 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	void search()
	{
		try
		{
			if(taSearchFor.getText().equals(""))
			{
				JOptionPane.showMessageDialog(this,"Please enter something to search for.","Vehicle Service",JOptionPane.OK_OPTION+JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				Vector results = new Vector();
				dbc.connect();
				sqlCommand = "SELECT * FROM Vehicle WHERE "+ cboCategory.getSelectedItem().toString() + " LIKE '"+ taSearchFor.getText()+"%'";

				ResultSet rs = dbc.select(sqlCommand);

				boolean notEmpty = false;

				if(rs.next())
				{
					results.add(rs.getString("VehicleNo"));
					results.add(rs.getString("Make"));
					results.add(rs.getString("Model"));
					results.add(rs.getString("Registration"));
					results.add(rs.getString("Route"));
					
				}

				//Check if any results were retrieved
					if(results.size() > 0)
					{
						
						results.add("Click the VehicleNo that appears on the results textfield to view the information on the textboxes!!");
					}
					else
				    {
					    results.add("No results found.");
				    }

				//Put results in listbox
				VehicleGUI.results.setListData(results);

				dbc.disconnect();
				this.dispose();
			}
		}
		catch(Exception exc)
		{
			System.err.println(exc.getMessage());
		}
	}
//	 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public void actionPerformed(ActionEvent ae)
	{
		Object src = ae.getSource();
		if(src == jbSearch)
		{
			try
			{
				search();
			}
			catch(Exception exc)
			{
				System.err.println(exc.getMessage());
			}
		}
		else if(src == jbCancel)
		{
			this.dispose();
		}
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
