/**
 * 
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.sql.*;
public class login extends JFrame implements ActionListener{
	//creating JLabels
	JLabel jbUser = new JLabel("UserName:");         
	JLabel jbPass = new JLabel("Password:");
	JLabel jbHeader = new JLabel("User Login",JLabel.CENTER);
    //creating JTextFields
	JTextField taUser = new JTextField(10);          
	JPasswordField taPass = new JPasswordField(10);
    //creating JButtons
	JButton jbLogin = new JButton("Login");          
	JButton jbCancel = new JButton("Cancel");
	//creating GridBagConstraints & Layout
	GridBagLayout gbl = new GridBagLayout();         
	GridBagConstraints gbc = new GridBagConstraints();
	DBConn dbc = new DBConn();
	String sqlCommand = "";
//*****************************************************************************************
	void buildConstraints(GridBagConstraints gbc,int gx,int gy,int gw,int gh,int wx,int wy)
	{
		gbc.gridx = gx;
		gbc.gridy = gy;
		gbc.gridwidth = gw;
		gbc.gridheight = gh;
		gbc.weightx = wx;
		gbc.weighty = wy;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.BOTH;
	}
//*****************************************************************************************
	login()
	{
		super("User Login");
		setSize(400,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel_1 = new JPanel();
		panel_1.setLayout(gbl);

		//Center Frame
		Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension size = getSize();
		
		scrnSize.height = scrnSize.height/2;
		scrnSize.width = scrnSize.width/2;
		
		size.height = size.height/2;
		size.width = size.width/2;
		
		int y = scrnSize.height - size.height;
		int x = scrnSize.width - size.width;
		setLocation(x, y);

		Image icon = Toolkit.getDefaultToolkit().getImage("Pics/home.gif");
		this.setIconImage(icon);

		//jbHeader
		buildConstraints(gbc,0,0,1,1,1,1);
		gbl.setConstraints(jbHeader,gbc);
		gbc.anchor = GridBagConstraints.CENTER;
		panel_1.add(jbHeader);
		Font customFont = new Font("Comic Sans MS", Font.BOLD,32);
		jbHeader.setFont(customFont);

		//jbUser
		buildConstraints(gbc,0,1,1,1,1,1);
		gbl.setConstraints(jbUser,gbc);
		panel_1.add(jbUser);

		//jbPass
		buildConstraints(gbc,0,2,1,1,1,1);
		gbl.setConstraints(jbPass,gbc);
		panel_1.add(jbPass);

		//taUser
		buildConstraints(gbc,1,1,1,1,1,1);
		gbl.setConstraints(taUser,gbc);
		panel_1.add(taUser);

		//taPass
		buildConstraints(gbc,1,2,1,1,1,1);
		gbl.setConstraints(taPass,gbc);
		panel_1.add(taPass);
		taPass.setEchoChar('x');

		//jbLogin
		buildConstraints(gbc,0,3,1,1,1,1);
		gbl.setConstraints(jbLogin,gbc);
		panel_1.add(jbLogin);
		jbLogin.addActionListener(this);
		jbLogin.setMnemonic('L');

		//jbCancel
		buildConstraints(gbc,1,3,1,1,1,1);
		gbl.setConstraints(jbCancel,gbc);
		panel_1.add(jbCancel);
		jbCancel.setMnemonic('C');
		jbCancel.addActionListener(this);

		setContentPane(panel_1);
		setVisible(true);
	}
//*****************************************************************************************
	public void actionPerformed(ActionEvent ae)
	{
		Object src = ae.getSource();

		String userName = taUser.getText().trim();
		String password = String.valueOf(taPass.getPassword()).trim() ;
		String user;
		String pass;
		boolean accept = false;
		if(src == jbLogin)
		{
			dbc.connect();
			try
			{
			sqlCommand = "SELECT * FROM UserManager WHERE Username='" + userName + "' AND Password='" + password + "'";
			ResultSet rs = dbc.select(sqlCommand);
			 if (rs.next())
			 {
				 VehicleService.showMain();
			 }
				else
				{
					JOptionPane.showMessageDialog(null,"Login failed, please make sure Username or Password is right","Vehicle Services",JOptionPane.OK_OPTION + JOptionPane.ERROR_MESSAGE);
					taPass.setText("");
					taPass.grabFocus();
				}
			}
			catch(Exception exc)
			{
				System.out.println("testing");
				System.err.println(exc.getMessage());
			}
			//dbc.disconnect();
		}
		else if (src == jbCancel)
		{
			System.exit(0);
		}
	}
//*****************************************************************************************
	public static void main(String[] args)throws Exception
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			splashVehicleScreen spv = new splashVehicleScreen();
			//spv.showSplash();

			//Robot rbt = new Robot();
			//rbt.delay(3000);

			//spv.dispose();

			login log = new login();
			//log.dispose();
		}
		catch(Exception exc)
		{
			System.out.println("Error " + exc.getMessage());
		}
	}
}
//*****************************************************************************************
   